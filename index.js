const log4js =require('log4js');

const logger= log4js.getLogger();
logger.level = "error";

logger.debug("Iniciando aplicaciòn en modo de pruebas.");
logger.info("La app ha iniiado correctamente");
logger.warn("Falta archivo config de la app");
logger.error("No se pudo acceder al sistema de archivos del equipo");
logger.fatal("Aplicacion no se pudo ejecutar en el so")

function sumar(x, y){
    return x+y;
}

module.exports=sumar;